<?php

/**
 * @file
 * Displays a month in the archive.
 *
 * Available variables:
 * - $month_name: The translated name of the month (string).
 * - $month_count: The number of posts in that month (int or string).
 * - $posts: HTML for all the posts in the month (string).
 *
 * @see template_preprocess_simple_archive_month()
 *
 * @ingroup themeable
 */
?>
<li class="month"><span><?php print $month_name; ?> (<?php print $month_count; ?>)</span>
  <ul>
    <?php print $posts; ?>
  </ul>
</li>
