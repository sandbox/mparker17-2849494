<?php

/**
 * @file
 * Displays a year in the archive.
 *
 * Available variables:
 * - $year_name: The name of the year (string).
 * - $year_count: The number of posts in the year (int or string).
 * - $months: HTML for the months in this year (string).
 *
 * @see template_preprocess_simple_archive_year()
 *
 * @ingroup themeable
 */
?>
<li class="year"><span><?php print $year_name; ?> (<?php print $year_count; ?>)</span>
  <ul>
    <?php print $months; ?>
  </ul>
</li>
