/**
 * @file
 * Client-side script for the simple_archive module.
 */
(function ($) {

  "use strict";

  Drupal.behaviors.simple_archive = {
    attach: function (context, settings) {
      $('.simple_archive', context).once('simple_archive_setup', function () {
        var $collapsable_elements = $('.year, .month');

        // Set up an event handler on the span inside the collapsable element.
        $collapsable_elements.children('span').click(function (eventObject) {
          var $current_object = $(this);
          var current_state = $current_object.parent().attr('data-collapsed');

          // If the element is already collapsed, expand it.
          if (current_state == 'collapsed') {
            $current_object.parent().attr('data-collapsed', 'expanded');
          }
          // If the element is already expanded, collapse it.
          else if (current_state == 'expanded') {
            $current_object.parent().attr('data-collapsed', 'collapsed');
          }
          // If we don't have a state already, then something went wrong during
          // initialization; or something else is messing with our elements, so
          // do nothing.
          else {
            // No-op.
          }
        });

        // Collapse everything by default.
        $collapsable_elements.attr('data-collapsed', 'collapsed');
      });
    }
  };

})(jQuery);
