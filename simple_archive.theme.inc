<?php

/**
 * @file
 * Theme functions for the simple_archive module.
 */

/**
 * Prepares variables for whole archive templates.
 *
 * Default template: simple-archive-widget.tpl.php.
 *
 * @param array &$variables
 *   - result_list: A flat array of views results.
 */
function template_preprocess_simple_archive_widget(array &$variables) {
  if (!empty($variables['result_list'])) {
    // Add our module's JS and CSS.
    $module_path = drupal_get_path('module', 'simple_archive');
    drupal_add_js($module_path . '/simple_archive.js');
    drupal_add_css($module_path . '/simple_archive.css');

    $result_tree = _simple_archive_result_list_to_tree($variables['result_list']);

    foreach ($result_tree as $year_name => $year_info) {
      $months = array();
      $year_count = 0;

      foreach ($year_info as $month_name => $month_info) {
        $posts = array();

        foreach ($month_info as $nid => $title) {
          $posts[] = theme('simple_archive_post', array(
            'post_title' => $title,
            'post_url' => url('node/' . $nid),
          ));
        }

        $year_count += count($posts);
        $months[] = theme('simple_archive_month', array(
          'month_name' => $month_name,
          'month_count' => count($posts),
          'posts_array' => $posts,
        ));
      }

      $variables['years_array'][] = theme('simple_archive_year', array(
        'year_name' => $year_name,
        'year_count' => $year_count,
        'months_array' => $months,
      ));
    }
  }
}

/**
 * Prepares variables for whole archive templates.
 *
 * Default template: simple-archive-widget.tpl.php.
 *
 * @param array &$variables
 *   - years_array: An array of HTML strings for years.
 *   - separator: A string containing a separator for each year.
 */
function template_process_simple_archive_widget(array &$variables) {
  $variables['years'] = implode((string) $variables['separator'], (array) $variables['years_array']);
}

/**
 * Prepares variables for year templates.
 *
 * Default template: simple-archive-year.tpl.php.
 *
 * @param array &$variables
 *   - months_array: An array of HTML strings for months.
 *   - separator: A string containing a separator for each month.
 */
function template_process_simple_archive_year(array &$variables) {
  $variables['months'] = implode((string) $variables['separator'], (array) $variables['months_array']);
}

/**
 * Prepares variables for month templates.
 *
 * Default template: simple-archive-month.tpl.php.
 *
 * @param array &$variables
 *   - posts_array: An array of HTML strings for posts.
 *   - separator: A string containing a separator for each post.
 */
function template_process_simple_archive_month(array &$variables) {
  $variables['posts'] = implode((string) $variables['separator'], (array) $variables['posts_array']);
}

/**
 * Prepares variables for post templates.
 *
 * Default template: simple-archive-post.tpl.php.
 *
 * @param array &$variables
 *   - post_title: The title of the post (string).
 *   - post_url: The URL to link to (string, optional).
 *   - link_options: An array of options to pass to l().
 */
function template_preprocess_simple_archive_post(array &$variables) {
  // By default, output the title of the node.
  $variables['post_text'] = (string) $variables['post_title'];

  // If we have a URL, link the title.
  if (!empty($variables['post_url'])) {
    $variables['post_text'] = l($variables['post_text'], (string) $variables['post_url'], (array) $variables['link_options']);
  }
}
