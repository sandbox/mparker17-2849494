<?php

/**
 * @file
 * Displays a post in the archive.
 *
 * Available variables:
 * - $post_text: HTML to display for this post (string).
 *
 * @see template_preprocess_simple_archive_post()
 *
 * @ingroup themeable
 */
?>
<li class="post"><?php print $post_text; ?></li>
