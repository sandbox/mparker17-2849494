<?php

/**
 * @file
 * Displays a whole archive.
 *
 * Available variables:
 * - $years: HTML for the years in this archive (string).
 *
 * @see template_preprocess_simple_archive_widget()
 *
 * @ingroup themeable
 */
?>
<ul class="simple_archive">
  <?php print $years; ?>
</ul>
