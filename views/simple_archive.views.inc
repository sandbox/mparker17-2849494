<?php

/**
 * @file
 * Views functions for the simple_archive module.
 */

/**
 * Implements hook_views_plugins().
 */
function simple_archive_views_plugins() {
  $plugins = array();

  // Define a custom style plugin.
  $plugins['style']['simple_archive'] = array(
    'title' => t('Simple archive'),
    'help' => t('Displays results for month and year in a tree.'),
    'handler' => 'simple_archive_plugin_style_simple_archive',
    'uses options' => TRUE,
    'uses row plugin' => TRUE,
    'uses grouping' => FALSE,
    'uses row class' => TRUE,
    'type' => 'normal',
    'path' => drupal_get_path('module', 'simple_archive'),
  );

  return $plugins;
}
