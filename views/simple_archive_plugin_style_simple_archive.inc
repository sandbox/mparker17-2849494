<?php

/**
 * @file
 * Contains \simple_archive_plugin_style_simple_archive.
 */

/**
 * A views style plugin to display a simple year-and-month archive.
 */
class simple_archive_plugin_style_simple_archive extends views_plugin_style {

  /**
   * {@inheritdoc}
   */
  public function render_grouping_sets($sets, $level = 0) {
    // Most of this is copied from \views_plugin_style::render_grouping_sets().
    $output = '';

    foreach ($sets as $set) {
      $row = reset($set['rows']);

      // Render as a grouping set.
      if (is_array($row) && isset($row['group'])) {
        $output .= theme(views_theme_functions('views_view_grouping', $this->view, $this->display), array(
          'view' => $this->view,
          'grouping' => $this->options['grouping'][$level],
          'grouping_level' => $level,
          'rows' => $set['rows'],
          'title' => $set['group'],
        ));
      }
      // Render as a record set.
      else {
        if ($this->uses_row_plugin()) {
          foreach ($set['rows'] as $index => $row) {
            $this->view->row_index = $index;
            $set['rows'][$index] = $this->row_plugin->render($row);
          }
        }

        // This part was changed from
        // \views_plugin_style::render_grouping_sets() to explicitly call the
        // simple_archive_widget theme function. This allows us to use its
        // default variables defined in simple_archive_theme().
        $output .= theme('simple_archive_widget', array(
          'result_list' => $this->view->result,
        ));
      }
    }

    unset($this->view->row_index);

    return $output;
  }

}
